require('./styles.scss');

export default class MainScreen {

    loadingBlock: HTMLElement | null = null;
    contentBlock: HTMLElement | null = null;

    constructor() {
        // If the app is loaded show the logo and hide the Loading indicator
        window.addEventListener('load', () => {
            this.hideLoadingIndicator();
            this.showContent();
        });
    }

    /*
     * Hide the Loading indicator
     */
    hideLoadingIndicator(): void {
        this.loadingBlock = window.document.getElementById('loading');
        if (this.loadingBlock) {
            this.loadingBlock.classList.add('hidden');
        }
    }

    /*
     * Show the content block, add some sample content
     */
    showContent(): void {
        this.contentBlock = window.document.getElementById('content');
        if (this.contentBlock) {
            this.contentBlock.innerHTML = '<img id="logo" src="assets/logo.svg" alt="Logo" >'
                + '<h1>Starter Kit</h1><h2>Frontend</h2><p>Webpack</p><p>TypeScript</p><p>SASS</p><p>Jest</p><p>Linters</p>';
            this.contentBlock.classList.remove('hidden');
        }
    }

}

new MainScreen();
