import fs = require('fs');
import path = require('path');
import MainScreen from './index';

const html: string = fs.readFileSync(path.resolve(__dirname, '../dist/index.html'), 'utf8');

jest.dontMock('fs');

const mainScreen: MainScreen = new MainScreen();

describe('Main content', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html.toString();
    });

    afterEach(() => {
        jest.resetModules();
    });

    it('A1 | renders the loading indicator', () => {
        expect(document.getElementById('loading')).toBeTruthy();
    });

    it('A2 | should load the logo', () => {
        mainScreen.hideLoadingIndicator();
        mainScreen.showContent();
        expect(document.getElementById('logo')).toBeTruthy();
    });
});
