## 1.1.1 (24 Aug 2021)
- Fixed the issue with the ESLint configs.

## 1.1.0 (22 Aug 2021)
- Updated dependencies.
- Extended ESLint rules.

## 1.0.0 (12 Dec 2020)
The Starter Kit was assembled from some pieces of other projects and moved into one repository.
