<div style="text-align: center" >

![logo](assets/logo.svg)
<h1 style="margin: -.5em 0 -1.3em" >STARTER KIT</h1>
<h2>FRONTEND</h2>

</div><br>

Starter kit for a web project's frontend.

<details>
<summary>Webpack 5</summary>

| package | description |
| --- | --- |
| `webpack` | The main module bundler for the project. Has the *dev*, *prod* and *common* configs. *Plugins* and *Loaders* are in the separate files, out of the main config file. |
| `webpack-cli` | The command-line interface for the Webpack. Needed to run the Webpack watcher. |
| `webpack-merge` | Needed to merge the Webpack configs from different files. |
| `clean-webpack-plugin` | Simply clean the build folder. |
| `copy-webpack-plugin` | Copies assets and some files to the build directory. |

</details>

<details>
<summary>Typescript 4</summary>

| package | description |
| --- | --- |
| `typescript` | To compile TS into JS. |
| `ts-loader` | Webpack loader for the **.ts* files. |

</details>

<details>
<summary>Jest 26</summary>

| package | description |
| --- | --- |
| `jest` | Testing framework for TS and JS. |
| `@types/jest` | Type definitions for Jest. |
| `ts-jest` | TS preprocessor for Jest so tests can be written in TS. |
| `jest-transform-stub` | Jest doesn't handle non-JS assets by default. This module helps to avoid errors when importing some assets - e.g. importing *main styles* right into *index.ts*. |

</details>

<details>
<summary>SASS</summary>

| package | description |
| --- | --- |
| `sass` | JS compiler SASS > CSS. |
| `sass-loader` | Webpack loader for **.sass* and **.scss* files. |
| `postcss` | A tool to transform CSS using various plugins. |
| `postcss-loader` | Webpack loader to process CSS with *PostCSS*. |
| `autoprefixer` | *PostCSS* plugin to parse CSS and add vendor prefixes to CSS rules. |
| `css-loader` | Reads styles as a string and interprets *@import* and *url()* like *import/require()* and will resolve them. |
| `css-minimizer-webpack-plugin` | Uses `cssnano` to optimize and minify your CSS. |
| `mini-css-extract-plugin` | Extracts CSS into separate files. It creates a CSS file per JS file which contains CSS. |

</details>

<details>
<summary>HTML5</summary>

| package | description |
| --- | --- |
| `html-webpack-plugin` | To generate, include hashed links to dependencies and minify the **.html* files based on the templates (e.g. *index.html*). |

</details>

<details>
<summary>JSON</summary>

| package | description |
| --- | --- | 
| `copy-webpack-plugin` | Copies the *manifest.json* to the build directory. |
| `file-loader` | Webpack loader for **.json* files. |

</details>

<details>
<summary>Linters</summary>

| package | description |
| --- | --- |
| `eslint` | *ECMAScript* linter. |
| `eslint-webpack-plugin` | Webpack loader for `eslint` to replace `eslint-loader`. |
| `@typescript-eslint/eslint-plugin` | Lint rules for the `eslint` to support *TypeScript*. |
| `@typescript-eslint/parser` | An `eslint` parser which leverages *TypeScript ESTree* to allow for `eslint` to lint *TypeScript* source code. |
| `eslint-config-airbnb-base` and `eslint-config-airbnb-typescript` | ESLint rules as a good practice example. |
| `stylelint` | Linter for styles. |
| `stylelint-config-sass-guidelines` | A `stylelint` config to support *SASS*. |
| `htmlhint` | The static code analysis tool for the *HTML*. |

</details>

<details>
<summary>Assets</summary>

| package | description |
| --- | --- |
| `copy-webpack-plugin` | Copies the */assets* folder to the build directory. |
| `file-loader` | Webpack loader for images, fonts, icons etc. |

</details>


### Set up

Prerequirements: `node v14 || v15`

To install the dependencies just run: `npm ci`

### Use

| command | description |
| --- | --- |
| `npm run dev` | Compile the code and run the *Webpack* watcher. |
| `npm run test` | Run the *Jest* tester which uses compiled files from the build folder. |
| `npm run lint`<br> `npm run lint:ts`<br> `npm run lint:styles`<br> `npm run lint:html` | Run code linters: all or separate - for *TypeScript*, *styles* or *HTML*. |
| `npm run build` | Compile the production-ready code and put it into the build folder (`/dist` by default). |
| `npm run clean`<br> `npm run clean:all` | Clean files and folders: just `/coverage` and `/dist` or all the generated, including `/node_modules` and `package-lock.json`. |

### Maintainers

- [Alex Rudol](https://alexrudol.com) – [me@alexrudol.com](mailto:me@alexrudol.com?subject=[GitLab]%20Contributing%20Starter%20Kit%20-%20Frontend)

### License

[BEERWARE License (Revision 42)](LICENSE.md)
