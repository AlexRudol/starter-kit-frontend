/*
 * LOADERS
 * for the source files and assets
 */

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');
const stylelint = require('stylelint');

const TypeScriptLoader = {
    test: /\.ts$/,
    exclude: /node_modules/,
    loader: 'ts-loader',
    options: {
        configFile: __dirname + '/tsconfig.json'
    }
};

const StylesLoader = {
    test: /\.scss$/,
    use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        {
            loader: 'postcss-loader',
            options: {
                postcssOptions: {
                    plugins: [
                        autoprefixer,
                        stylelint({
                            configFile: __dirname + '/.stylelintrc'
                        })
                    ]
                }
            }
        },
        'sass-loader'
    ]
};

const ImagesLoader = {
    test: /\.(png|jpg|jpeg|webp|svg)$/,
    loader: 'file-loader',
    options: {
        outputPath: 'assets'
    }
};

const FontsLoader = {
    test: /\.(eot|otf|ttf|woff|woff2)$/,
    loader: 'file-loader',
    options: {
        outputPath: 'assets'
    }
};

const JSONLoader = {
    test: /\.json$/,
    type: 'javascript/auto',
    use: [
        {
            loader: 'file-loader',
            options: {
                name: '[name].[ext]'
            }
        }
    ]
};

module.exports = {
    TypeScriptLoader,
    StylesLoader,
    ImagesLoader,
    FontsLoader,
    JSONLoader
};
