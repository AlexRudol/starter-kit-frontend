module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    roots: [ '../src' ],
    moduleNameMapper: {
        '^.+\\.(css|style|less|sass|scss|png|jpg|ttf|woff|woff2|svg)$': 'jest-transform-stub'
    },
    collectCoverageFrom: [
        '../src/**/*.ts'
    ],
    coverageDirectory: '../coverage'
};
