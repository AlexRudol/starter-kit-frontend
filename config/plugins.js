/*
 * PLUGINS
 * and configurations
 */

// Helpers
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');
const ProgressPlugin = new webpack.ProgressPlugin();

// JavaScript
const _ESLintPlugin = require('eslint-webpack-plugin');
const ESLintPlugin = new _ESLintPlugin({
    overrideConfigFile: path.resolve(__dirname, '.eslintrc'),
    context: path.resolve(__dirname, '../src/js'),
    files: '**/*.js'
});

// Styles
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CSSMinimizerPlugin = require('css-minimizer-webpack-plugin');

// HTML
const _HTMLWebpackPlugin = require('html-webpack-plugin');
const HTMLWebpackPlugin = new _HTMLWebpackPlugin({
    template: '/src/index.html'
});

// Copying files
const _CopyPlugin = require('copy-webpack-plugin');
const CopyPlugin = new _CopyPlugin({
    patterns: [
        { from: 'src/manifest.json', to: 'manifest.json' },
        { from: 'assets', to: 'assets' }
    ]
});

module.exports = {
    CleanWebpackPlugin,
    ProgressPlugin,
    ESLintPlugin,
    MiniCssExtractPlugin,
    CSSMinimizerPlugin,
    HTMLWebpackPlugin,
    CopyPlugin
};
