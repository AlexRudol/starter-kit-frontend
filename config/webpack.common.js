const path = require('path');
const loaders = require('./loaders');
const plugins = require('./plugins');

module.exports = {
    entry: path.resolve(__dirname, '../src/index.ts'),
    resolve: {
        extensions: [ '.html', '.scss', '.ts' ]
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, '../dist'),
        assetModuleFilename: 'assets/[hash][ext][query]'
    },
    module: {
        rules: [
            loaders.TypeScriptLoader,
            loaders.StylesLoader,
            loaders.ImagesLoader,
            loaders.FontsLoader,
            loaders.JSONLoader
        ]
    },
    optimization: {
        minimize: true,
        minimizer: [
            new plugins.CSSMinimizerPlugin()
        ]
    },
    plugins: [
        new plugins.CleanWebpackPlugin(),
        plugins.ProgressPlugin,
        plugins.ESLintPlugin,
        new plugins.MiniCssExtractPlugin(),
        new plugins.CSSMinimizerPlugin(),
        plugins.HTMLWebpackPlugin,
        plugins.CopyPlugin
    ]
};
